package camt.se234.project;
import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;

public class SaleOrderServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;
    static List<SaleOrder> mockSaleOrders;


    @Before
    public void setUp(){
        orderDao = mock (OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);

        when(orderDao.getOrders()).thenReturn(mockSaleOrders);
    }
    @BeforeClass
    public static void addData(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(01l).productId("p0001").name("Apple").description("good fruit for health").price(50).build());
        mockProducts.add(Product.builder().id(02l).productId("p0002").name("Banana").description("better fruit for health").price(570).build());
        mockProducts.add(Product.builder().id(03l).productId("p0003").name("Orange").description("Nothing good about it").price(590).build());
        mockProducts.add(Product.builder().id(04l).productId("p0004").name("Papaya").description("Use for salad").price(700).build());


        List<SaleTransaction> mockSaleTransaction = new ArrayList<>();
        mockSaleTransaction.add(SaleTransaction.builder().id(01l).transactionId("111").product(mockProducts.get(0)).amount(1).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(02l).transactionId("123").product(mockProducts.get(1)).amount(10).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(03l).transactionId("124").product(mockProducts.get(2)).amount(20).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(04l).transactionId("125").product(mockProducts.get(3)).amount(30).build());




        mockSaleOrders = new ArrayList<>();
        mockSaleOrders.add(SaleOrder.builder().id(011l).saleOrderId("111").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransaction.get(0));add(mockSaleTransaction.get(3));}}).build());
        mockSaleOrders.add(SaleOrder.builder().id(022l).saleOrderId("123").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransaction.get(2));add(mockSaleTransaction.get(1));}}).build());


    }

    @Test
    public void testGetSaleOrdersWithMock(){

        assertThat(saleOrderService.getSaleOrders(),hasItems(mockSaleOrders.get(0),mockSaleOrders.get(1)));

    }

    @Test
    public void testGetAverageSaleOrderPriceWithMock(){
        assertThat(saleOrderService.getAverageSaleOrderPrice(),is( 19275.0));

    }
}
