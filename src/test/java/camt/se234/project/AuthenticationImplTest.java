package camt.se234.project;
import camt.se234.project.dao.UserDao;
import camt.se234.project.dao.UserDaoImpl;
import camt.se234.project.entity.User;
import camt.se234.project.service.AuthenticationServiceImpl;
import com.sun.org.apache.xerces.internal.util.PropertyState;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationImplTest {
    UserDao userDao;
    AuthenticationServiceImpl authenticationService;
    @Before
    public void setup(){
        userDao= mock(UserDao.class);
        authenticationService  = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }
    @Test
    public void testWithMock(){
        User testUser1 = User.builder().id(000L).username("weiyuan").password("daixin").role("user").build();
        when(userDao.getUser("weiyuan","daixin")).thenReturn(testUser1);
        assertThat(authenticationService.authenticate("weiyuan","daixin"),is(testUser1));

        User testUser2 = User.builder().id(333L).username("weiyuan").password("daixin").role("user").build();
        when(userDao.getUser("weiyuan","daixin")).thenReturn(testUser1);
        assertThat(authenticationService.authenticate("daixin","weiyuan"),nullValue());
    }

}
