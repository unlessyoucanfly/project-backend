package camt.se234.project;
import camt.se234.project.dao.ProductDao;
import camt.se234.project.dao.ProductDaoImpl;
import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.Product;
import camt.se234.project.service.AuthenticationServiceImpl;
import camt.se234.project.service.ProductService;
import camt.se234.project.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
public class ProductServiceImplTest {
    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setup(){
        productDao= mock(ProductDao.class);
        productService  = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }
    @Test
    public void testgetAllProduct(){
        List<Product> mockProduct= new ArrayList<>();


        mockProduct.add(new Product(000L,"p0001","apple","good fruit for health","apple.jpg",50));
        mockProduct.add(new Product(001L,"p0002","banana","better fruit for health","banana.jpg",570));
        mockProduct.add(new Product(002L,"p0003","orange","Nothing good about it","orange.jpg",590));
        mockProduct.add(new Product(003L,"p0004","papaya","use for salad","papaya.jpg",700));
        mockProduct.add(new Product(004L,"p0005","peach","use for peach tea","peach.jpg",900));
        mockProduct.add(new Product(005L,"p0006","watermelon","use for drink","watermelon.jpg",-1));
        when(productService.getAllProducts()).thenReturn(mockProduct);
        assertThat(productService.getAllProducts(),hasItems(new Product(000L,"p0001","apple","good fruit for health","apple.jpg",50)));
        new Product(001L,"p0002","banana","better fruit for health","banana.jpg",570);
        new Product(002L,"p0003","orange","Nothing good for health","orange.jpg",590);
        new Product(003L,"p0004","payaya","use for salad","papaya.jpg",700);
        new Product(004L,"p0005","peach","use for peach tea","peach.jpg",900);
        new Product(005L,"p0006","watermelon","use dor drink","watermelon.jpg",-1);
    }
    @Test
    public void testgetAvailableProduct(){
        List<Product> mockProduct= new ArrayList<>();
        mockProduct.add(new Product(000L,"p0001","apple","good fruit for health","apple.jpg",50));
        mockProduct.add(new Product(001L,"p0002","banana","better fruit for health","banana.jpg",570));
        mockProduct.add(new Product(002L,"p0003","orange","Nothing good about it","orange.jpg",590));
        mockProduct.add(new Product(003L,"p0004","papaya","use for salad","papaya.jpg",700));
        mockProduct.add(new Product(004L,"p0005","peach","use for peach tea","peach.jpg",900));
        when(productService.getAvailableProducts()).thenReturn(mockProduct);
        assertThat(productService.getAvailableProducts(),hasItems(new Product(000L,"p0001","apple","good fruit for health","apple.jpg",50)));
        new Product(001L,"p0002","banana","better fruit for health","banana.jpg",570);
        new Product(002L,"p0003","orange","Nothing good for health","orange.jpg",590);
        new Product(003L,"p0004","payaya","use for salad","papaya.jpg",700);
        new Product(004L,"p0005","peach","use for peach tea","peach.jpg",900);
    }
    @Test
    public void testgetUnAvailableProduct(){
        List<Product> mockProduct= new ArrayList<>();
        mockProduct.add(new Product(005L,"p0006","watermelon","use for drink","watermelon.jpg",-1));
        when(productService.getAllProducts()).thenReturn(mockProduct);
        assertThat(productService.getUnavailableProductSize(),is(1));
    }
}
